# Guide to Amazon App Store

> How to publish an Android app to Amazon app store



## New applicationId

Amazon app store requires your app to have a unique `applicationId` across all different app stores (including Google Play Store).

The best/easisst way to do this is to use "product flavors" in gradle build script. For example,

    productFlavors {
        play {
            applicationIdSuffix ".play"
        }
        fire {
            applicationIdSuffix ".fire"
        }
    }

This way, you can deploy essentially the same app to multiple app stores.
On the other end of the spectrum, you can create a separate app for each app store.
(I prefer to do that for a large app: Create one or more shared libraries and use them in different app-store specific apps.)


## Building APK

The usual stuff when you create an Android app.
Remember to build an "unaligned" apk.

The APK needs to be signed with a persistent key:

    keytool -genkey -v -keystore my-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-alias

You can use different keys for different flavors.



## Image assets

Create icons and screenshots.

* Small icon: 114px x 114px
* Large icon: 512px x 512px
* Screenshots: (3 to 10) 800px × 480px, 1024px × 600px, 1280px × 720px, 1280px × 800px, 1920px × 1200px, 2560px × 1600px

You can take screenshots from real devices or emulators.


## App information

* App title (required):
* App SKU (optional):
* Category (required): 
* Category Refinements (optional—only shown if available): 
* Customer support contact (optional): 
* Customer Support email address (required): 
* Customer Support phone (required): 
* Customer Support website (required): 
* Privacy policy URL (optional): 



## App description

* Language (required): 
* Display Title (required): 
* Short description (required): The maximum length is 1,200 characters
* Long description (required): The maximum length is 4,000 characters.
* Product feature bullets (required): Type three to five key features of your app, one per line.
* Keywords (optional):




_TBD_



## References

* [Understanding Amazon Appstore Submission](https://developer.amazon.com/public/support/submitting-your-app/tech-docs/appstore-understanding-submission)
* [Submitting Apps to the Amazon Appstore](https://developer.amazon.com/public/support/submitting-your-app/tech-docs/submitting-apps-to-amazon-appstore)
* [Image Guidelines for App Submission](https://developer.amazon.com/public/support/submitting-your-app/tech-docs/asset-guidelines-for-app-submission)
* [Targeting Amazon Devices with Your Android Manifest](https://developer.amazon.com/public/support/submitting-your-app/tech-docs/targeting-amazon-devices-with-your-android-manifest)
* [Getting Started with Live App Testing](https://developer.amazon.com/public/resources/development-tools/live-app-testing/docs/getting-started-with-live-app-testing)
* [How to take screenshots in Android Studio](https://developer.android.com/studio/debug/am-screenshot.html)
* [Device art generator](https://developer.android.com/distribute/marketing-tools/device-art-generator.html)
* [Sign Your App](https://developer.android.com/studio/publish/app-signing.html)



